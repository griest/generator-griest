const Generator = require('yeoman-generator')
const chalk = require('chalk')
const yosay = require('yosay')

export default class extends Generator {
  prompting () {
    // Have Yeoman greet the user.
    this.log(yosay(
      `Welcome to the bedazzling ${chalk.green('web-library')} generator!`
    ))

    const prompts = [{
      type: 'input',
      name: 'name',
      message: 'Project name'
    }, {
      type: 'input',
      name: 'description',
      message: 'Project description'
    }, {
      type: 'input',
      name: 'namespace',
      message: 'Gitlab namespace',
      default: 'griest'
    }, {
      type: 'input',
      name: 'gitName',
      message: 'Gitlab project name',
      default: answers => answers.name
    }]

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props
    })
  }

  writing () {
    this.fs.copyTpl(
      this.templatePath('.'),
      this.destinationPath('.'),
      this.props
    )
  }

  install () {
    this.yarnInstall()
  }
}
