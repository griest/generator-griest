# generator-griest [![NPM version][npm-image]][npm-url] 
[![pipeline status](https://gitlab.com/griest/generator-griest/badges/master/pipeline.svg)](https://gitlab.com/griest/generator-griest/commits/master) [![Dependency Status][daviddm-image]][daviddm-url]
> My personal project generator.

## Installation

First, install [Yeoman](http://yeoman.io) and generator-griest using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-griest
```

Then generate your new project:

```bash
yo web-library
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Peter Lauck](https://gitlab.com/griest)


[npm-image]: https://badge.fury.io/js/generator-griest.svg
[npm-url]: https://npmjs.org/package/generator-griest
[daviddm-image]: https://david-dm.org/griest024/generator-griest.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/griest024/generator-griest
