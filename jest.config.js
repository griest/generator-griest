module.exports = {
  coverageDirectory: 'public/reports/coverage',
  coverageReporters: ['lcov']
};
